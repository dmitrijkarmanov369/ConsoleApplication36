﻿#include<iostream>
#include <windows.h>
#include<time.h>
using namespace std;
int main()
{
	setlocale(LC_ALL, "Russian");
	int level, a, b, magicNum, userNum;
	int userChoice, userLives, userPoints, guessed;
	userPoints = 0;
	guessed = 0;
	srand(time(NULL));
	do {
		cout << "Добро пожаловать в игру!\n";
		cout << "Твой выбор:\n";
		cout << "1 - первый уровень: magic number [1..10]\n";
		cout << "2 - второй уровень: magic number [10..100]\n";
		cout << "3 - выход\n";
		cin >> userChoice;
		switch (userChoice) {
		case 1:
		{
			cout << "Добро пожаловать на первый уровень: "
				"magic number [1..10]!\n";
			a = 1;
			b = 10;
			userLives = (b - a + 1) / 2;
			cout << "Бот считает число...\n";
			Sleep(1000);
			magicNum = a + rand() % (b - a);
			cout << "Загадываемое число готово! "
				"Давайте попробуем угадать его!\n";
			do {
				cout << "У вас есть на это ";
				cout << userLives << " жизни.\n";
				cout << "Попробуй угадать, твоё число?\n";
				cin >> userNum;
				if (userNum == magicNum)
				{
					cout << "Вы угадали загадываемое число!\n";
					guessed = 1;
				}
				else {
					cout << "Вы потеряли 1 жизнь!\n";
					userLives--;
					cout << "У вас осталось ";
					cout << userLives << "\n";
					do {
						cout << "Вы хотите получить "
							"подсказку? Это стоит "
							"1 жизнь!\n";
						cout << "Ваш выбор:\n";
						cout << "1 - да\n";
						cout << "0 - нет\n";
						cin >> userChoice;
						if (userChoice == 1)
						{
							if (userNum > magicNum)
							{
								cout << "Давайте дадим "
									"меньше...\n";
								userLives--;
							}
							else
							{
								cout << "Давайте дадим "
									"больше...\n";
								userLives--;
							}
						}
					} while ((userChoice != 1) &&
						(userChoice != 0));
				}
			} while ((userLives > 0) && (guessed == 0));
			userPoints = userLives * 5;
			if (userPoints == 0)
			{
				cout << "Вы проиграли!\n";
			}
			else
			{
				cout << "Ваш счёт:" << userPoints;
				cout << "\n";
			}
			break;
		}
		case 2:
		{
			cout << "Добро пожаловать на второй уровень: "
				"magic number [10..100]!\n";
			a = 10;
			b = 100;
			userLives = (b - a + 1) / 4;
			cout << "Бот загадывает число...\n";
			Sleep(1000);
			magicNum = a + rand() % (b - a);
			cout << "Загадываемое число готово! "
				"Давайте попробуем угадать его!\n";
			do {
				cout << "У вас есть на это ";
				cout << userLives << " жизни.\n";
				cout << "Попробуй угадать, твоё число?\n";
				cin >> userNum;
				if (userNum == magicNum)
				{
					cout << "Вы угадали загадываемое число!\n";
					guessed = 1;
				}
				else {
					cout << "Вы потеряли 1 жизнь!\n";
					userLives--;
					cout << "У вас осталось ";
					cout << userLives << "\n";
					do {
						cout << "Вы хотите получить "
							"подсказку? Это стоит "
							"1 жизнь!\n";
						cout << "Ваш выбор:\n";
						cout << "1 - да\n";
						cout << "0 - нет\n";
						cin >> userChoice;
						if (userChoice == 1)
						{
							if (userNum > magicNum)
							{
								cout << "Давайте дадим "
									"меньше...\n";
								userLives--;
							}
							else
							{
								cout << "Давайте дадим "
									"больше...\n";
								userLives--;
							}
						}
					} while ((userChoice != 1) &&
						(userChoice != 0));
				}
			} while ((userLives > 0) && (guessed == 0));
			userPoints = userLives * 10;
			if (userPoints == 0)
			{
				cout << "Вы проиграли!\n";
			}
			else
			{
				cout << "Ваш счёт:" << userPoints;
				cout << "\n";
			}
			break;
		}
		case 3:
		{
			cout << "Увидимся!";
			break;
		}
		default:
			cout << "Неправильный пункт меню!";
 }
 } while (userChoice != 3);
 return 0;
}